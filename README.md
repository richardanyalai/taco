# Taco

<div align="center" style="text-align:center">
	<img src="taco-logo.png" width="40%" style="max-width: 400px" />
</div>

## About

Taco is a simple cli TODO app written in Rust. It stores the list of items
sorted in alphabetical order in a file named `.taco` under the user's
home directory.

## Installing

Requirements:
Install [Rust](https://www.rust-lang.org/tools/install).

Building and installing is as simple as issuing the following command: `make`.

It gets installed in the folder `.local/bin` under the user's home directory.

## Usage

### CLI mode

```bash
taco + option + <item>
```

Options:

- a + \<item\> - add a new item
- c + \<item\> - check the item
- u + \<item\> - uncheck the item
- r + \<item\> - remove the item
- l - list all items

Example:

```bash
$ taco a lime
$ taco a lemon
$ taco a tomato
$ taco a onion
$ taco a avocado
$ taco a potato
$ taco a taco shells
$ taco c lime
$ taco c avocado
$ taco c onion
$ taco u onion
$ taco r potato
$ taco l
[X] avocado
[ ] lemon
[X] lime
[ ] onion
[ ] taco shells
[ ] tomato
```

### TUI mode

```bash
taco
```

The keyboard controls are almost the same as the flags in CLI mode with some
additional keybindings:

- j / s / down arrow - go down
- k / w / up arrow - go up
- g - go to the top
- G - go to the bottom
- t - toggle the checked / unchecked status
