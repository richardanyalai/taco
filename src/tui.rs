use crate::db::Database;
use ncurses::*;

const REG: i16 = 0;
const HIG: i16 = 1;

pub fn start(db: &mut Database) -> std::io::Result<()> {
	initscr();
	noecho();
	curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);
	use_default_colors();
	start_color();

	init_pair(REG, COLOR_WHITE, -1);
	init_pair(HIG, COLOR_BLACK, COLOR_WHITE);

	let mut running = true;
	let mut current_idx = 0_usize;
	let mut add_mode = false;
	let mut chars: Vec<u8> = vec![];
	let mut todos: Vec<String> = db.list();
	todos.sort();

	while running {
		for (idx, task) in todos.iter().enumerate() {
			let pair = if current_idx == idx { HIG } else { REG };

			attr_on(COLOR_PAIR(pair));
			mv(idx as i32, 0);
			addstr(&format!(
				"[{}] {}",
				if db.is_checked(task) { 'X' } else { ' ' },
				task
			));
			attr_off(COLOR_PAIR(pair));
		}

		refresh();

		let key = getch();

		if add_mode {
			match key as u8 {
				b'\n' => {
					db.insert(
						String::from_utf8(chars.clone()).unwrap(),
						false,
					)
					.flush()?;

					todos = db.list();
					todos.sort();

					add_mode = false;

					clear();

					continue;
				}
				8 | 127 => {
					chars.pop().unwrap();
				}
				_ => chars.push(key as u8),
			}

			mv(todos.len() as i32, 0);

			clrtoeol();

			addstr(&format!(
				"[ ] {}",
				String::from_utf8(chars.clone()).unwrap()
			));

			refresh();
		} else {
			match key as u8 as char {
				'q' => running = false,
				'k' | 'w' => {
					current_idx = if current_idx > 0 {
						current_idx - 1
					} else {
						todos.len() - 1
					}
				}
				'j' | 's' => {
					current_idx = if current_idx < todos.len() - 1 {
						current_idx + 1
					} else {
						0
					}
				}
				'g' => current_idx = 0,
				'G' => current_idx = todos.len() - 1,
				'c' => db.insert(todos[current_idx].clone(), true).flush()?,
				'u' => db.insert(todos[current_idx].clone(), false).flush()?,
				't' => {
					if db.is_checked(&todos[current_idx]) {
						db.insert(todos[current_idx].clone(), false).flush()?
					} else {
						db.insert(todos[current_idx].clone(), true).flush()?
					}
				}
				'd' | 'x' | 'r' => {
					db.remove(todos[current_idx].clone()).flush()?;
					todos.remove(current_idx);

					current_idx = if current_idx < todos.len() {
						current_idx
					} else {
						current_idx - 1
					};

					clear();
				}
				'a' => {
					chars = vec![];

					mv(todos.len() as i32, 0);

					current_idx = todos.len();

					addstr(&format!(
						"[ ] {}",
						String::from_utf8(chars.clone()).unwrap()
					));

					refresh();

					add_mode = true;
				}
				_ => {
					if key == 27 && getch() as u8 == 91 {
						match getch() as u8 {
							65 => {
								current_idx = if current_idx > 0 {
									current_idx - 1
								} else {
									todos.len() - 1
								}
							}
							66 => {
								current_idx = if current_idx < todos.len() - 1
								{
									current_idx + 1
								} else {
									0
								}
							}
							_ => (),
						}
					}
				}
			}
		}
	}

	endwin();

	Ok(())
}
