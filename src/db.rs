use std::{collections::HashMap, fs, io};

pub struct Database<'a> {
	db_path: &'a str,
	map: HashMap<String, bool>,
}

impl<'a> Database<'a> {
	pub fn new(db_path: &'a str) -> Result<Database<'a>, io::Error> {
		let mut map = HashMap::new();

		if !std::path::Path::new(db_path).exists() {
			fs::File::create(db_path)?;
		}

		let contents = fs::read_to_string(db_path)?;

		for line in contents.lines() {
			let chunks = line.trim().split(' ').collect::<Vec<&str>>();
			let len = chunks.len();

			if len >= 2 {
				let key = chunks[0..len - 1].join(" ");
				let value = chunks[len - 1];

				map.insert(key.to_owned(), value == "true");
			}
		}

		Ok(Database { db_path, map })
	}

	pub fn list(&self) -> Vec<String> {
		let mut vec = vec![];
		let mut keys: Vec<String> = self.map.keys().cloned().collect();

		keys.sort();

		for key in keys {
			vec.push(key);
		}

		vec
	}

	pub fn is_checked(&self, key: &str) -> bool {
		*self.map.get(key).unwrap()
	}

	pub fn insert(&mut self, key: String, value: bool) -> &mut Self {
		self.map.insert(key, value);
		self
	}

	pub fn remove(&mut self, key: String) -> &mut Self {
		self.map.remove(&key);
		self
	}

	pub fn flush(&mut self) -> io::Result<()> {
		let mut contents = String::new();

		for (key, value) in &self.map {
			contents.push_str(&format!("{} {}\n", key, value));
		}

		fs::write(self.db_path, contents)
	}
}
