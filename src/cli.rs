use crate::db::Database;

pub fn start(
	args: &mut std::env::Args,
	db: &mut Database,
) -> std::io::Result<()> {
	let operation = args.nth(1).unwrap();
	let chunks = args.collect::<Vec<String>>();
	let task = chunks[0..chunks.len()].join(" ");

	match &operation[..] {
		"add" | "a" => db.insert(task, false).flush(),
		"check" | "c" => db.insert(task, true).flush(),
		"uncheck" | "u" => db.insert(task, false).flush(),
		"del" | "rm" | "d" | "r" => db.remove(task).flush(),
		"list" | "l" => {
			for entry in db.list() {
				println!(
					"[\x1b[1;32m{}\x1b[0m] {}",
					if db.is_checked(&entry) { 'X' } else { ' ' },
					entry
				);
			}
			Ok(())
		}
		_ => panic!("Invalid parameters"),
	}
}
