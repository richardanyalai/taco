mod cli;
mod db;
mod tui;

fn main() -> std::io::Result<()> {
	let db_path = &format!("{}/.taco", std::env::var("HOME").unwrap())[..];
	let mut db = db::Database::new(db_path).expect("Creating database failed");
	let mut args = std::env::args();

	if args.len() > 1 {
		cli::start(&mut args, &mut db)
	} else {
		tui::start(&mut db)
	}
}
