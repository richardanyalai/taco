all: install

install:
	@cargo clean --release
	@cargo build --release
	@mkdir -p ${HOME}/.local/bin
	@cp ./target/release/taco ${HOME}/.local/bin
